FROM node:14.4-alpine

RUN apk update
RUN apk upgrade

WORKDIR /app
COPY . /app

RUN apk add --no-cache --virtual .build-deps \
    python \
    make \
    g++ \
    git && \
    npm install && \
    apk del .build-deps

RUN  adduser fern --disabled-password
USER fern

CMD npm run start
